const Messages = require('../models/messages')

module.exports = (io) => {
//
  // const nsp = io.of('/my-namespace')
  // nsp.on('connection', function (socket) {
  //   console.log('someone connected')
  // })
  // nsp.emit('hi', 'everyone!')

  io.on('connection', (socket) => {
//
    socket.on('login', data => {
      // console.log(data)
      data.groups.forEach((i) => {
        socket.join(i.groupId)
      })
    })

    socket.on('message', data => {
      // console.log(data)
      let message = new Messages({
        message: data.message,
        date: data.date,
        user: data.user,
        group: data.group
      })

      message.save()
        .catch((err) => console.log(err))
        .then((message) => {
          io.to(data.group).send(data)
        })

      // io.to('sample').send(data)
    // socket.send(data)
      // console.log(data)
    })
  })
}
