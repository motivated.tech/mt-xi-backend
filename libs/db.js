const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')

mongoose.Promise = global.Promise

const connection = mongoose.connect('mongodb://admin:admin@ds137340.mlab.com:37340/xi')

autoIncrement.initialize(connection)

mongoose.connection.on('error', console.error.bind(console, 'connection error:'))

mongoose.connection.once('open', function () {
  console.log('connected to db')
})
