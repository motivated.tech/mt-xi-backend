const gulp = require('gulp')
const server = require('gulp-develop-server')

gulp.task('default', ['server:start'], () => {})

// run server
gulp.task('server:start', () => {
  server.listen({ path: './server.js' })
  gulp.watch([ './*.js', './routes/*.js', './controllers/*.js', './libs/*.js', './models/*.js' ], server.restart)
})
