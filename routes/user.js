const router = require('express').Router()
const user = require('../controllers/user')
// const guard = require('express-jwt-permissions')()
// const check = require('../utils/check')

// Get All Users
router.get('/', (req, res) => {
  user.getAllUsers()
    .catch((err) => { return res.status(500).send(err) })
    .then((users) => res.json(users))
})

// Get single User by username
router.get('/:username', (req, res) => {
  user.getOneUser(req.params.username)
    .catch((err) => { return res.status(500).send(err) })
    .then(user => res.json(user))
})

// Create new User
router.post('/', (req, res) => {
  user.newUser(req.body)
    .catch((err) => { return res.status(500).send(err) })
    .then((user) => res.json(user))
})

// Update
router.put('/:username', (req, res) => {
  user.updateUser(req.params.username, req.body)
    .catch((err) => { return res.status(500).send(err) })
    .then((user) => res.json(user))
})

// Delete
router.delete('/', (req, res) => {
  user.deleteUser(req.body.username)
    .catch((err) => { return res.status(500).send(err) })
    .then((val) => res.json(val))
})

module.exports = router
