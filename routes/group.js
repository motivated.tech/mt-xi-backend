const router = require('express').Router()
const group = require('../controllers/group')

router.get('/', (req, res) => {
  group.getAllGroups()
    .catch((err) => { return res.status(500).send(err) })
    .then((groups) => res.json(groups))
})

router.get('/:groupId', (req, res) => {
  group.getOneGroup(req.params.groupId)
    .catch((err) => { return res.status(500).send(err) })
    .then((groups) => res.json(groups))
})

// Add Group to Course
router.post('/', (req, res) => {
  group.newGroup(req.body)
    .catch((err) => { return res.status(500).send(err) })
    .then((group) => res.json(group))
})

// TODO: create route to for group update

router.post('/:groupId/add_member', (req, res) => {
  group.addMemberToGroup(req.params.groupId, req.body.username)
    .catch((err) => { return res.status(500).send(err) })
    .then((group) => res.json(group))
})

module.exports = router
