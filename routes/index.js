
const baseurl = ''

module.exports = (app) => {
  app.use(`${baseurl}/user`, require('./user'))
  app.use(`${baseurl}/group`, require('./group'))
  app.use(`${baseurl}/messages`, require('./messages'))
  app.use(`${baseurl}/tasks`, require('./tasks'))
  app.use(`${baseurl}/token`, require('./token'))
}
