const router = require('express').Router()
const messages = require('../controllers/messages')

router.get('/', (req, res) => {
  messages.getAllMessagessByGroup(req.body.groupId, req.body.limit, req.body.skip)
    .catch((err) => { return res.status(500).send(err) })
    .then((messagess) => res.json(messagess))
})

router.get('/:messageId', (req, res) => {
  messages.getOneGroup(req.params.messageId)
    .catch((err) => { return res.status(500).send(err) })
    .then((message) => res.json(message))
})

router.post('/', (req, res) => {
  messages.newMessage(req.body)
    .catch((err) => { return res.status(500).send(err) })
    .then((messages) => res.json(messages))
})

// TODO: create route to for messages update

router.post('/:messagesId/add_member', (req, res) => {
  messages.addMemberToGroup(req.params.messagesId, req.body.username)
    .catch((err) => { return res.status(500).send(err) })
    .then((messages) => res.json(messages))
})

module.exports = router
