const Group = require('../models/group')
const user = require('../controllers/user')

module.exports = {

  getAllGroups () {
    return new Promise((resolve, reject) => {
      Group.find({}, '-_id -__v')
        .populate('members', '-_id -password -groups -__v')
        .sort('groupId')
        .catch((err) => reject(err))
        .then((groups) => resolve(groups))
    })
  },

  getOneGroup (groupId) {
    return new Promise((resolve, reject) => {
      Group.find({ groupId }, '-_id -__v')
      .populate('members', '-_id -password -groups -__v')
        .catch((err) => reject(err))
        .then((groups) => resolve(groups))
    })
  },

  newGroup (newGroup) {
    return new Promise((resolve, reject) => {
      let group = new Group({
        name: newGroup.name
      })

      group.save()
        .catch((err) => reject(err))
        .then((group) => {
          resolve(group)
        })
    })
  },

  addMemberToGroup (groupId, username) {
    return new Promise((resolve, reject) => {
      user.getOneUser(username)
        .catch((err) => reject(err))
        .then((user) => {
          Group.findOne({ groupId })
            .catch((err) => reject(err))
            .then((group) => {
              if (group.members.indexOf(user._id) === -1) {
                group.members.push(user._id)
                // group.save()
                  // .catch((err) => reject(err))
                  // .then((group) => resolve(group))
              } else {
                resolve({error: 'USER_IS_MEMBER', message: 'user is member'})
              }

              if (user.groups.indexOf(group._id) === -1) {
                user.groups.push(group._id)
                user.save()
                  .catch((err) => reject(err))
                  // .then((user) => resolve(user))
                group.save()
                  .catch((err) => reject(err))
                  .then((group) => {
                    group.members = undefined
                    group._id = undefined
                    group.__v = undefined
                    resolve(group)
                  })
              } else {
                resolve({error: 'USER_IS_MEMBER', message: 'user is member'})
              }
            })
        })
    })
  }

}
