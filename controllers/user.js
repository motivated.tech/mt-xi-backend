const User = require('../models/user')

module.exports = {
  getAllUsers () {
    return new Promise((resolve, reject) => {
      User.find({}, '-password -_id -__v')
      .populate('groups', '-_id -members -__v')
      .catch((err) => reject(err))
      .then((users) => resolve(users))
    })
  },

  getOneUser (username) {
    return new Promise((resolve, reject) => {
      User.findOne({ username }, '-password')
      .catch((err) => reject(err))
      .then((user) => {
        if (!user) resolve({ error: 'NO_USER_FOUND', message: 'no user found' })
        resolve(user)
      })
    })
  },

  newUser (newUser) {
    return new Promise((resolve, reject) => {
      let user = new User({
        username: newUser.username,
        password: newUser.password,
        name: newUser.name ? { first: newUser.name.first, last: newUser.name.last } : undefined,
        email: newUser.email,
        permissions: newUser.permissions ? newUser.permissions : undefined,
        pic: newUser.pic
      })
      user.save()
      .catch((err) => reject(err))
      .then((user) => {
        user.password = undefined
        user._id = undefined
        resolve(user)
      })
    })
  },

  updateUser (username, userUpdates) {
    return new Promise((resolve, reject) => {
      User.findOne({ username })
        .catch((err) => reject(err))
        .then((user) => {
          user.username = userUpdates.username ? userUpdates.username : user.username
          user.password = userUpdates.password ? userUpdates.password : user.password
          user.name = userUpdates.name ? { first: userUpdates.name.first, last: userUpdates.name.last } : user.name
          user.email = userUpdates.email ? userUpdates.email : user.email
          user.permissions = userUpdates.permissions ? userUpdates.permissions : user.permissions

          user.save()
            .catch((err) => reject(err))
            .then((user) => {
              user.password = undefined
              user._id = undefined
              resolve(user)
            })
        })
    })
  },

  deleteUser (username) {
    return new Promise((resolve, reject) => {
      User.remove({ username })
      .catch((err) => reject(err))
      .then((val) => {
        if (val.result.ok === 1 && val.result.n === 1) {
          resolve({ result: 'OK', message: `User ${username} successfully deleted` })
        } else {
          resolve({ result: 'FAIL', message: `User ${username} not found` })
        }
      })
    })
  }

}
