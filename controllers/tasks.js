const Tasks = require('../models/tasks')

module.exports = {

  getAllTasksByGroup (groupId, limit, skip) {
    return new Promise((resolve, reject) => {
      Tasks.find({group: groupId}, '-_id')
        .limit(limit)
        .skip(skip)
        // .sort('-messageId')
        .catch((err) => reject(err))
        .then((groups) => resolve(groups))
    })
  },

// TODO
  getOneMessages (messageId) {
    return new Promise((resolve, reject) => {
      Messages.find({ messageId }, '-_id')
        .populate('members', '-_id -password')
        .catch((err) => reject(err))
        .then((groups) => resolve(groups))
    })
  },

  newTask (newTasks) {
    return new Promise((resolve, reject) => {
      let task = new Tasks({
        title: newTasks.title,
        description: newTasks.description,
        date: newTasks.date,
        users: newTasks.user,
        group: newTasks.group
      })

      task.save()
        .catch((err) => reject(err))
        .then((task) => {
          resolve(task)
        })
    })
  },

// TODO: finish method
  completeTask (newMessage) {
    return new Promise((resolve, reject) => {
      let message = new Messages({
        message: newMessage.message,
        date: newMessage.date,
        user: newMessage.user,
        group: newMessage.group
      })

      message.save()
        .catch((err) => reject(err))
        .then((message) => {
          resolve(message)
        })
    })
  },

  addMemberToMessages (groupId, username) {
    return new Promise((resolve, reject) => {
      user.getOneUser(username)
        .catch((err) => reject(err))
        .then((user) => {
          Messages.findOne({ groupId })
            .catch((err) => reject(err))
            .then((group) => {
              if (group.members.indexOf(user._id) === -1) {
                group.members.push(user._id)
                // group.save()
                  // .catch((err) => reject(err))
                  // .then((group) => resolve(group))
              } else {
                resolve({error: 'USER_IS_MEMBER', message: 'user is member'})
              }

              if (user.groups.indexOf(group._id) === -1) {
                user.groups.push(group._id)
                user.save()
                  .catch((err) => reject(err))
                  // .then((user) => resolve(user))
                group.save()
                  .catch((err) => reject(err))
                  .then((group) => resolve(group))
              } else {
                resolve({error: 'USER_IS_MEMBER', message: 'user is member'})
              }
            })
        })
    })
  }

}
