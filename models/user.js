const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')
const ObjectId = mongoose.Schema.Types.ObjectId
const bcrypt = require('bcrypt-nodejs')

const UserSchema = new mongoose.Schema({
  username: { type: String, unique: true, required: true },
  password: { type: String, required: true },
  name: {
    first: { type: String },
    last: { type: String }
  },
  email: { type: String },
  pic: { type: String },
  permissions: { type: Array },
  groups: [{ type: ObjectId, ref: 'Group' }]
})

// convert password to salt before saving it to the db
UserSchema.pre('save', function (callback) {
  let user = this
  if (!user.isModified('password')) { return callback() }
  bcrypt.genSalt(5, (err, salt) => {
    if (err) { return callback(err) }
    bcrypt.hash(user.password, salt, null, (err, hash) => {
      if (err) { return callback(err) }
      user.password = hash
      callback()
    })
  })
})

// compare password to salt
UserSchema.methods.verifyPassword = function (password, callback) {
  bcrypt.compare(password, this.password, function (err, isMatch) {
    if (err) { return callback(err) }
    callback(null, isMatch)
  })
}

UserSchema.plugin(autoIncrement.plugin, {
  model: 'User',
  field: 'userId',
  startAt: 100,
  incrementBy: 1
})

module.exports = mongoose.model('User', UserSchema)
