const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')

const MessageSchema = new mongoose.Schema({
  message: { type: String },
  date: { type: Date },
  user: { type: String }, // username
  group: { type: Number } // groupId
})

MessageSchema.plugin(autoIncrement.plugin, {
  model: 'Message',
  field: 'messageId',
  startAt: 100,
  incrementBy: 1
})

module.exports = mongoose.model('Message', MessageSchema)
