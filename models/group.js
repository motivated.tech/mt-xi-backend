const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')
const ObjectId = mongoose.Schema.Types.ObjectId

const GroupSchema = new mongoose.Schema({
  name: { type: String },
  members: [{ type: ObjectId, ref: 'User' }]
  // messages
})

GroupSchema.plugin(autoIncrement.plugin, {
  model: 'Group',
  field: 'groupId',
  startAt: 100,
  incrementBy: 1
})

module.exports = mongoose.model('Group', GroupSchema)
