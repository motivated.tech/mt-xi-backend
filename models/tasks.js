const mongoose = require('mongoose')
const autoIncrement = require('mongoose-auto-increment')
const ObjectId = mongoose.Schema.Types.ObjectId

const TaskSchema = new mongoose.Schema({
  title: { type: String },
  description: { type: String },
  completed: { type: Boolean },
  date: { type: Date },
  users: [{ type: ObjectId, ref: 'User' }],
  group: { type: Number } // groupId
})

TaskSchema.plugin(autoIncrement.plugin, {
  model: 'Task',
  field: 'taskId',
  startAt: 100,
  incrementBy: 1
})

module.exports = mongoose.model('Task', TaskSchema)
