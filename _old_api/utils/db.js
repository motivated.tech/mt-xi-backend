var mongoose = require('mongoose')
mongoose.Promise = global.Promise;

mongoose.connect('mongodb://admin:admin@ds145158.mlab.com:45158/mt-backend')

mongoose.connection.on('error', console.error.bind(console, 'connection error:'))

mongoose.connection.once('open', () => {
  console.log('Successfully connected to the DB!')
})