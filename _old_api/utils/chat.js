var Chat = require('../models/chat')

module.exports = (server) => {

var WebSocketServer = require('uws').Server;
var wss = new WebSocketServer({ path: 'test', server });

function onMessage(message) {
    console.log('received: ' + message);
}

wss.on('connection', function(ws) {
	console.log('connected')
	// console.log(ws)
    ws.on('message', onMessage);
    ws.send('something');
});

}
