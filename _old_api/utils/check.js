const jwt = require('jsonwebtoken')
const parseToken = require('parse-bearer-token')

module.exports = {
	isAdmin(req) {
		return jwt.decode(parseToken(req)).permissions == 'admin'
	},
	isSameUser(req) {
		return jwt.decode(parseToken(req)).userId == req.params.user_id
	}
}