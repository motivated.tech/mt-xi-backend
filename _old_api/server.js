const express = require('express')
const app = express()
/*----------  sorter versio of the two lines above  ----------*/
// const app = require('express')()

const server = require('http').Server(app)
const cors = require('cors')
const bodyParser = require('body-parser')
const expressJWT = require('express-jwt')
const auth = require('./config/auth')


/*----------  general configs  ----------*/
app.use(cors())
app.use(bodyParser.json())

/*----------  static user profile to be substituted with NGINX  ----------*/
app.use('/user_pics', express.static('user_pics'))

/*----------  authetication  ----------*/
app.use(expressJWT({ secret: auth.secret }).unless({path: ['/api/token']}))

/*----------  chat  ----------*/
require('./utils/chat')(server)

/*----------  initialize DB  ----------*/
require('./utils/db')

/*----------  Routes  ----------*/
require('./routes/_routes')(app)


/*----------  error handling for JWT  ----------*/
app.use(function (err, req, res, next) {
	if (err.name === 'UnauthorizedError') {
		res.status(401).send('invalid token...')
	}
})

/*----------  error handling for JWT permissions  ----------*/
// This might be obsolete soon
app.use(function (err, req, res, next) {
  if (err.code === 'permission_denied') {
    res.status(401).send('insufficient permissions')
  }
})

/*----------  Start the service  ----------*/
server.listen(8081)
