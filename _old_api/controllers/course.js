var Course = require('../models/course')
var group = require('../controllers/group')
var chat = require('../controllers/chat')
var ids = require('../utils/ids')

function courseId2course_id() {
		Course.findOne({courseId})
		.catch((err) => reject(err))
		.then((course) => resolve(course._id))
	}

module.exports = {

	getAllCourses() {
		return new Promise((resolve, reject) => {
			Course.find({}, '-_id')
			.populate('groups', '-_id name groupId')
			.catch((err) => reject(err))
			.then((courses) => resolve(courses))
		})	
	},

	getOneCourse(courseId) {
		return new Promise((resolve, reject) => {
			Course.findOne({ courseId })
			.populate('groups', '-_id name groupId')
			.catch((err) => reject(err))
			.then((course) => {
				if (!course) resolve({error: 'NO_COURSE_FOUND', message: "no course found"})
					resolve(course)
			})
		})	
	},

	newCourse(newCourse) {
		return new Promise((resolve, reject) => {
			ids.getNewCourseId().then((newCourseId) => {
				var course = new Course({
					id: `c${newCourseId}`,
					name: newCourse.name,
					description: newCourse.description
				})

				course.save()
				.catch((err) => { return reject(err) })
				.then((course) => {

					group.newGroup(course)
					.catch((err) => reject(err))
					.then((group) => {

						course.groups.push(group._id)
						course.save()
						.catch((err) => reject(err))


					// New Chat registered to group
					chat.newChat(group._id)
					.catch((err) => reject(err))
					.then((chat) => {

						group.chats.push(chat._id)

						group.save()
						.catch((err) => reject(err))
						.then((group) => {
							resolve(course)
						})
					})
				})



				})
			})
		})	
	},

	updateCourse(courseId, courseUpdates) {
		return new Promise((resolve, reject) => {
			Course.findOne({ courseId })
			.catch((err) => reject(err))
			.then((course) => {
				if (!course) resolve({error: 'NO_COURSE_FOUND', message: "no course found"})

					course.name = courseUpdates.name
				course.description = courseUpdates.description

				course.save()
				.catch((err) => { return reject(err) })
				.then((course) => {
					course._id = undefined
					resolve(course)
				})

			})
		})	
	},

	deleteCourse(courseId) {
		return new Promise((resolve, reject) => {
			Course.remove({ courseId })
			.catch((err) => reject(err))
			.then((val) => {
				if (val.result.ok == 1 && val.result.n == 1) {
					resolve({ message: `Course ${courseId} successfully deleted`})
				} else {
					resolve({ message: `Course ${courseId} not found`})
				}
			})
		})
	},

	addGroupToCourse(courseId) {
		return new Promise((resolve, reject) => {

			// Find Course
			Course.findOne({courseId})
			.catch((err) => reject(err))
			.then((course) => {
				// New Group registered to course
				group.newGroup(course)
				.catch((err) => reject(err))
				.then((group) => {
					
					course.groups.push(group._id)
					course.save()
					.catch((err) => reject(err))


					// New Chat registered to group
					chat.newChat(group._id)
					.catch((err) => reject(err))
					.then((chat) => {

						group.chats.push(chat._id)

						group.save()
						.catch((err) => reject(err))
						.then((group) => {
							resolve(course)
						})
					})
				})
			})
		}
		)},



	}
