const User = require('../models/user')
const ids = require('../utils/ids')
const jdenticon = require('../utils/jdenticon')
const bcrypt = require('bcrypt-nodejs')

module.exports = {
	getAllUsers() {
		return new Promise((resolve, reject) => {
			User.find({}, '-password -_id')
			.catch((err) => reject(err))
			.then((users) => resolve(users))
		})
		
	},

	getOneUser(userId) {
		return new Promise((resolve, reject) => {
			User.findOne({ userId }, '-password')
			.catch((err) => reject(err))
			.then((user) => {
				if (!user) resolve({error: 'NO_USER_FOUND', message: "no user found"})
					resolve(user)
			})
		})
	},

	newUser(newUser) {
		return new Promise((resolve, reject) => {
			ids.getNewUserId().then((newUserId) => {
				let user = new User({
					id: `u${newUserId}`,
					username: newUser.username,
					password: newUser.password,
					name: newUser.name ? { first: newUser.name.first, last: newUser.name.last } : undefined,
					email: newUser.email,
					permissions: newUser.permissions ? newUser.permissions : undefined
				})
				user.save()
				.catch((err) => reject(err))
				.then((user) => {
					jdenticon(user.username, 200)
					user.password = undefined
					user._id = undefined
					resolve(user)
				})
			}).catch((err) => reject(err))
		})
	},

	newUsers(newUsers) {
		return new Promise((resolve, reject) => {
			ids.getNewUsersIds(newUsers.length).then((newUserId) => {
				
				/*----------  prepare newUsers object  ----------*/
				newUsers.forEach((i) => {
					i.id = `u${newUserId++}`
					i.password = bcrypt.hashSync(i.password, bcrypt.genSaltSync(5), null)
					jdenticon(i.username, 200)
				})
				User.insertMany(newUsers)
				.catch((err) => reject(err))
				.then((users) => resolve(users))
				
			})
		})
	},

	updateUser(userId, userUpdates) {
		return new Promise((resolve, reject) => {
			User.findOne({ userId })
			.catch((err) => reject(err))
			.then((user) => {
				user.username = userUpdates.username
				user.password = userUpdates.password
				user.name = {
					first: userUpdates.name.first,
					last: userUpdates.name.last,
				}
				user.email = userUpdates.email
				user.permissions = userUpdates.permissions ? userUpdates.permissions : user.permissions

				user.save()
				.catch((err) => reject(err))
				.then((user) => {
					user.password = undefined
					user._id = undefined
					resolve(user)
				})
			})
		})
	},

	deleteUser(userId) {
		return new Promise((resolve, reject) => {
			User.remove({ userId })
			.catch((err) => reject(err))
			.then((val) => {
				if (val.result.ok == 1 && val.result.n == 1) {
					resolve({ result: 'OK', message: `User ${userId} successfully deleted`})
				} else {
					resolve({ result: 'FAIL', message: `User ${userId} not found`})
				}
			})
		})
	}

}
