const Chat = require('../models/chat')
const group = require('../controllers/group')
const getNewChatId = require('../utils/ids').getNewChatId

module.exports = {

	getAllChats() {
		Chat.find({}, '-_id')
		.catch((err) => { return res.status(500).send(err) })
		.then((chats) => res.json(chats))
	},

	getOneChat(chatId) {
		Chat.findOne({chatId}, '-_id')
		.catch((err) => { return res.status(500).send(err) })
		.then((chat) => res.json(chat))
	},

	newChat(group_id) {
		return new Promise((resolve, reject) => {
			// New Chat
			getNewChatId().then((newChatId) => {
				var chat = new Chat({
					id: `ch${newChatId}`,
					group: group_id
				})

				chat.save()
				.catch((err) => reject(err))
				.then((chat) => {
					resolve(chat)
				})	
			})
		})
	}

}
