const Group = require('../models/group')
const course = require('../controllers/course')
const chat = require('../controllers/chat')
const user = require('../controllers/user')
const getNewGroupId = require('../utils/ids').getNewGroupId

module.exports = {

	getAllGroups() {
		return new Promise((resolve, reject) => {
			Group.find({}, '-_id')
			.populate('course', '-_id name courseId')
			.populate('chats', '-_id -group -messages')
			.populate('members', '-_id -password')
			.catch((err) => reject(err))
			.then((groups) => resolve(groups))
		})
	},

	getOneGroup(courseId) {
		return new Promise((resolve, reject) => {
			Group.find({ courseId }, '-_id')
			.populate('course', '-_id name courseId')
			.populate('chats', '-_id -group -messages')
			.populate('members', '-_id -password')
			.catch((err) => reject(err))
			.then((groups) => resolve(groups))
		})
	},

	newGroup(course) {
		return new Promise((resolve, reject) => {
			getNewGroupId().then((newGroupId) => {
				let group = new Group({
					id: `g${newGroupId}`,
					name: `Group ${course.groups.length + 1} for Course ${course.courseId}`,
					course: course._id
				})

				group.save()
				.catch((err) => reject(err))
				.then((group) => {
					resolve(group)
				})
			})
		})
	},


	// addGroupToCourse(courseId) {
	// 	return new Promise((resolve, reject) => {

	// 		course.getOneCourse(courseId)
	// 		.catch((err) => reject(err))
	// 		.then((course) => {
	// 			// course._id exist
	// 			// New Group
	// 			getNewGroupId().then((newGroupId) => {
	// 				let group = new Group({
	// 					groupId: `g${newGroupId}`,
	// 					name: `Group ${course.groups.length + 1} for Course ${course.courseId}`,
	// 					course: course._id
	// 				})

	// 				group.save()
	// 				.catch((err) => reject(err))
	// 				.then((group) => {
	// 				// group._id exist
	// 				course.groups.push(group._id)
	// 				course.save()
	// 				.catch((err) => reject(err))

	// 				resolve(group)
	// 			})
	// 			})
	// 		})
	// 	}
	// 	)},


	addMemberToGroup(groupId, userId) {

		return new Promise((resolve, reject) => {
			user.getOneUser(userId)
			.catch((err) => reject(err))
			.then((user) => {
					//user._id exist
					Group.findOne({ groupId })
					.catch((err) => reject(err))
					.then((group) => {
						if (group.members.indexOf(user.id) == -1 ) {
							group.members.push(user._id)
							group.save()
							.catch((err) => reject(err))
							.then((group) => resolve(group))
						}
						else {
							reject({error: "USER_IS_MEMBER", message: 'user is member'})
						}
					})
				})
		})
	}

}
