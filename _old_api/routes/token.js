var router = require('express').Router()
var jwt = require('jsonwebtoken')
var auth = require('../config/auth')
var User = require('../models/user')

router.post('/', (req, res) => {
    User.findOne({ username: req.body.username }, '-_id', function (err, user) {
      if (err) { return res.status(500).send(err) }

      // // No user found with that username
      if (!user) { return res.status(401).json({error: 'INVALID_USER', message: 'invalid user'}) }

      // Make sure the password is correct
      user.verifyPassword(req.body.password, function(err, isMatch) {
        if (err) { return res.status(500).send(err) }

        // Password did not match
        if (!isMatch) { return res.status(401).json({error: 'INVALID_PASSWORD', message: 'wrong password'}) }

        // Success
    	let token = jwt.sign({userId: user.userId, permissions: user.permissions }, auth.secret)
        user.password = undefined
        res.json({id_token: token, user})
      })
    })

})

module.exports = router
