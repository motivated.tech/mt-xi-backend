const router = require('express').Router()
const user = require('../controllers/user')
const guard = require('express-jwt-permissions')()
const check = require('../utils/check')

// Get All Users
router.get('/', guard.check('admin'), (req, res) => {
		user.getAllUsers()
		.catch((err) => { return res.status(500).send(err) })
		.then((users) => res.json(users))
})

// Get single User by id
router.get('/:user_id', (req, res) => {
	if (check.isAdmin(req)) {
		user.getOneUser(req.params.user_id)
		.catch((err) => { return res.status(500).send(err) })
		.then((user) => {
			user._id = undefined
			res.json(user)
		})
	} 

	if (check.isSameUser(req)) {
		user.getOneUser(req.params.user_id)
		.catch((err) => { return res.status(500).send(err) })
		.then((user) => res.json(user))
	}

	if (!check.isSameUser(req) && !check.isAdmin(req)) {
		res.status(401).send('insufficient permissions')
	}

})

// Create new User
router.post('/', guard.check('admin') ,(req, res) => {
	if (Array.isArray(req.body)) {
		user.newUsers(req.body)
		.catch((err) => console.log(err))
		.then((users) => res.json(users))

	}
	if (!Array.isArray(req.body)) {
	user.newUser(req.body)
	.catch((err) => { return res.status(500).send(err) })
	.then((user) => res.json(user))
}
})

// Update
router.put('/:user_id', (req, res) => {
	if (check.isAdmin(req)) {
		user.updateUser(req.params.user_id, req.body)
		.catch((err) => { return res.status(500).send(err) })
		.then((user) => res.json(user))
	}

	if (check.isSameUser(req)) {
		req.body.permissions = undefined
		user.updateUser(req.params.user_id, req.body)
		.catch((err) => { return res.status(500).send(err) })
		.then((user) => res.json(user))
	}

	if (!check.isSameUser(req) && !check.isAdmin(req)) {
		res.status(401).send('insufficient permissions')
	}
})

// Delete
router.delete('/:user_id', guard.check('admin'), (req, res) => {
	user.deleteUser(req.params.user_id)
	.catch((err) => { return res.status(500).send(err) })
	.then((val) => res.json(val))
})

module.exports = router

