/*----------  namespace for the API  ----------*/
const baseurl = '/api'

module.exports = (app) => {

	app.get('/', function (req, res) { res.send('Hey there flaca!!') })

// Routes
app.use(`${baseurl}/user`, require('./user'))
app.use(`${baseurl}/course`, require('./course'))
app.use(`${baseurl}/group`, require('./group'))
app.use(`${baseurl}/chat`, require('./chat'))
app.use(`${baseurl}/token`, require('./token'))
	// app.use('/play', require('./play'))
}