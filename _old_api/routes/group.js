var router = require('express').Router()
var group = require('../controllers/group')
const guard = require('express-jwt-permissions')()
const check = require('../utils/check')

router.get('/', (req, res) => {
	group.getAllGroups()
	.catch((err) => { return res.status(500).send(err) })
	.then((groups) => res.json(groups))
})

// Add Group to Course
router.post('/new_group', (req, res) => {
	group.addGroupToCourse(req.body.courseId)
	.catch((err) => { return res.status(500).send(err) })
	.then((group) => res.json(group))
})

router.post('/add_member', (req, res) => {
	group.addMemberToGroup(req.body.groupId ,req.body.userId)
	.catch((err) => { return res.status(500).send(err) })
	.then((group) => res.json(group))
})


module.exports = router