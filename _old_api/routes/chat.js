var router = require('express').Router()
var chat = require('../controllers/chat')


router.get('/', (req, res) => {
	chat.getAllChats()
	.catch((err) => { return res.status(500).send(err) })
	.then((chats) => res.json(chats))
})

router.get('/:chat_id', (req, res) => {
	chat.getOneChat(req.params.chat_id)
	.catch((err) => { return res.status(500).send(err) })
	.then((chat) => res.json(chat))
})

router.post('/', (req, res) => {
	Group.findOne({groupId: req.body.group})
	.catch((err) => { return res.status(500).send(err) })
	.then((group) => {
		if (!group) { res.status(500).json({message: "no group found"}) }
			
		getNewChatId().then((newChatId) => {
			var chat = new Chat({
				chatId: `ch${newChatId}`,
				group: group._id
			})

			chat.save()
			.catch((err) => { return res.status(500).send(err) })
			.then((chat) => {
				group.chats.push(chat._id)
				group.save()
				.catch((err) => { return res.status(500).send(err) })
				res.json(chat)
			})	
		})
	})
})

module.exports = router