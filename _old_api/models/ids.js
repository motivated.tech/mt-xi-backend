var mongoose = require('mongoose')

var IDsSchema = new mongoose.Schema({
	version: {type: Number, unique: true},
	courseId: { type: Number },
	userId: { type: Number },
	groupId: { type: Number },
	chatId: { type: Number }
})

module.exports = mongoose.model('IDs', IDsSchema)
