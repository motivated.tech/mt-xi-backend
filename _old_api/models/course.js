const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

const CourseSchema = new mongoose.Schema({
	id: { type: String, required: true, unique: true },
	name: { type: String, required: true },
	description: { type: String },
	groups: [{ type: ObjectId, ref: 'Group' }],
})

module.exports = mongoose.model('Course', CourseSchema)
