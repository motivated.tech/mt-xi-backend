const mongoose = require('mongoose')
const ObjectId = mongoose.Schema.Types.ObjectId

const GroupSchema = new mongoose.Schema({
	id: { type: String, required: true, unique: true },
	name: { type: String},
	course: { type: ObjectId, ref: 'Course', required: true },
	chats: [{ type: ObjectId, ref: 'Chat' }],
	members: [{ type: ObjectId, ref: 'User' }],
})

module.exports = mongoose.model('Group', GroupSchema)
