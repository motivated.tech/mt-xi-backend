const app = require('express')()
const server = require('http').Server(app)
const io = require('socket.io')(server)
const cors = require('cors')
const bodyParser = require('body-parser')

require('./libs/db')
require('./libs/chat')(io)

app.use(cors())
app.use(bodyParser.json())

app.get('/', function (req, res) {
  res.send('hello')
})

require('./routes')(app)

server.listen(8081)
